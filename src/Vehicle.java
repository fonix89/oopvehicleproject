public abstract class Vehicle implements Movable, FuelConsumer {

    private int prodYear;
    private double weight;
    private double speed;
    private double fuelPerKM;
    private Road road;


    public Vehicle(int prodYear, double weight, double speed,
                   double fuelPerKM, Road road) {
        this.prodYear = prodYear;
        this.weight = weight;
        this.speed = speed;
        this.fuelPerKM = fuelPerKM;
        this.road = road;
    }

    public int getProdYear() {
        return prodYear;
    }

    public double getWeight() {
        return weight;
    }

    public double getSpeed() {
        return speed;
    }

    public double getFuelPerKM() {
        return fuelPerKM;
    }

    public Road getRoad(){
        return road;
    }

    @Override
    public double move(int time) {
        return time * speed;
    }

    @Override
    public double FuelUsage(double distance) {

        double roadTempCoef = (100+25-road.getTemperature())/100;
        double roadQualityCoef = 1-road.getQuality();

        return distance * fuelPerKM * roadQualityCoef * roadTempCoef;
    }
}
