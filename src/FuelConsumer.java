public interface FuelConsumer {
    double FuelUsage(double distance);
}
